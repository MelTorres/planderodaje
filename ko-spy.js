﻿/*Binding handler to spy on an element and determine when it comes into the viewport*/
ko.bindingHandlers.scrollSpy = {
    init: function (element, valueAccessor) {
   
        var height = window.innerHeight;
        var obsv = valueAccessor();
		
        //listen for scroll events
        function onScrollEventHandler(event) {
            //get our reference point
            var rect = element.getBoundingClientRect();
            if (rect.top > -(element.offsetHeight) && rect.top < height) {
                if(!obsv()){
                   obsv(true);	
                }
            } else {
            	if(obsv()){
            	 obsv(false);	
            	}
            }
        }

        //attach the listener
        var el = window;
        if (el.addEventListener)
            el.addEventListener('scroll', onScrollEventHandler, false);
        else if (el.attachEvent)
            el.attachEvent('onscroll', onScrollEventHandler);

    }
};