﻿/*Parallax binding handler that updates an elements backgroundPosition based on its height and
 * posistion relative to its parent using getBoundingClientRect()
*/
ko.bindingHandlers.parallax = {
    init: function (element) {

        var height = window.innerHeight,
            pos = 0,
            rect,
            el;

        //listen for scroll events
        function onScrollEventHandler(event) {

            rect = element.getBoundingClientRect();
            if (rect.top > -(element.offsetHeight) && rect.top < height) {
                pos = (rect.top / 20);
                element.style.backgroundPosition = "50% " + pos + "px";
            }

        }

        //attach the listener
        el = window;
        if (el.addEventListener)
            el.addEventListener('scroll', onScrollEventHandler, false);
        else if (el.attachEvent)
            el.attachEvent('onscroll', onScrollEventHandler);

    }
};